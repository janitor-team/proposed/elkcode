#!/bin/bash

(cd tests; bash ./tests_quick.sh /usr/bin/elk-lapw)
for i in tests/test-*/test.log; do	\
  if [ -e $$i ]; then			\
    echo "Error for test $$i:";		\
    sed -n -e '/^Error/,$$p' $$i;	\
  fi					\
done
