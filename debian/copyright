Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: elk
Upstream-Contact: http://sourceforge.net/p/elk/discussion/
Source: http://elk.sourceforge.net

Files: *
Copyright: 2002-2013 F. Bultmark, F. Cricchio, L. Nordstrom, J. K. Dewhurst,
 S. Sharma, C. Ambrosch-Draxl and E. K. U. Gross 
           Partial Copyright 2010 M. G. Blaber
License: GPL-3+

Files: debian/*
Copyright: 2013 Michael Banck <mbanck@debian.org>
           2013,201414 Daniel Leidert <dleidert@debian.org>
           Partial Copyright 2014 Marcin Dulak <Marcin.Dulak@gmail.com>
License: GPL-3+

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

Files: src/brzint.f90 src/clebgor.f90 src/connect.f90 src/energykncr.f90
 src/euler.f90 src/factnm.f90 src/factr.f90 src/fderiv.f90 src/findband.f90
 src/findlambdalu.f90 src/findsymlat.f90 src/flushifc.f90 src/fsmooth.f90
 src/fxc_pwca.f90 src/gaunt.f90 src/gauntyry.f90 src/gcd.f90
 src/genbeffmt.f90 src/genppts.f90 src/genrlm.f90 src/genylm.f90
 src/grad2rfmt.f90 src/gradrfmt.f90 src/gradzfmt.f90 src/hermite.f90
 src/i3mdet.f90 src/i3minv.f90 src/i3mtv.f90 src/libxc.f90 src/libxc.f90
 src/libxc.f90 src/lopzflm.f90 src/main.f90 src/main.f90 src/mixadapt.f90
 src/mixander.f90 src/mixbroyden.f90 src/mixpulay.f90 src/polynom.f90
 src/r3cross.f90 src/r3frac.f90 src/r3mdet.f90 src/r3minv.f90 src/r3mm.f90
 src/r3mmt.f90 src/r3mtm.f90 src/r3mtv.f90 src/r3mv.f90 src/radnucl.f90
 src/rdiracdme.f90 src/rdirac.f90 src/rdiracint.f90 src/rdmdtsdn.f90
 src/rdmengyxc.f90 src/rdmvaryn.f90 src/rdmwritededn.f90 src/readalphalu.f90
 src/reciplat.f90 src/rfinterp.f90 src/rfirvec.f90 src/rfmtctof.f90
 src/rfmtinp.f90 src/rhomagsh.f90 src/rlmrot.f90 src/rotaxang.f90
 src/rotrflm.f90 src/rotzflm.f90 src/rschrodapp.f90 src/rschroddme.f90
 src/rschrodint.f90 src/rtozflm.f90 src/sbesseldm.f90 src/sbessel.f90
 src/sdelta.f90 src/sdelta_fd.f90 src/sdelta_lr.f90 src/sdelta_mp.f90
 src/sdelta_sq.f90 src/sortidx.f90 src/sphcover.f90 src/sphcrd.f90
 src/spline.f90 src/stheta.f90 src/stheta_fd.f90 src/stheta_mp.f90
 src/stheta_sq.f90 src/testcheck.f90 src/timesec.f90 src/vecfbz.f90
 src/wigner3j.f90 src/wigner6j.f90 src/writechg.f90 src/writeengy.f90
 src/writeldapu.f90 src/writesf.f90 src/xc_am05.f90 src/xc_pwca.f90
 src/xc_pzca.f90 src/xc_vbh.f90 src/ylmrot.f90 src/ylmroty.f90
 src/z2mctm.f90 src/z2mmct.f90 src/z2mm.f90 src/zflmconj.f90 src/zfmtinp.f90
 src/zher2a.f90 src/zher2b.f90 src/ztorflm.f90 src/spacegroup/r3cross.f90
 src/spacegroup/r3frac.f90 src/spacegroup/r3minv.f90 src/spacegroup/r3mm.f90
 src/spacegroup/r3mv.f90
Copyright: 2002-2012 F. Bultmark, F. Cricchio, L. Nordstrom, J. K. Dewhurst,
 S. Sharma, C. Ambrosch-Draxl and E. K. U. Gross
License: LGPL3

Files: src/xc_wc06.f90 src/x_wc06.f90
Copyright: 2006 Zhigang Wu and R. E. Cohen
License: LGPL3

Files: src/xc_xalpha.f90
Copyright: 1998-2006 ABINIT group (DCA, XG, GMR).
License: LGPL3

License: LGPL3
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
Comment:
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-3".

Files: src/BLAS/*
Copyright: None
License: public-domain
 These files have been put into public domain.

Files: src/LAPACK/*
Copyright: 1992-2011 The University of Tennessee and The University of
 Tennessee Research Foundation
           2000-2011 The University of California Berkeley
           2006-2012 The University of Colorado Denver
License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
